//
// Sensor de Luminosidade
// Vídeo de teste: https://www.instagram.com/p/C0sjrLYR44B
// 
// Testado no Arduino Uno
// 
// Por Ericson Benjamim @pardalmaker
//

// Pino analógico que o sensor está conectado
const int ldr = A0;

// Pino Referente ao led ou rele
const int luz = 11; // troque este valor por D1 se usar um ESP8266

// valor que sera armazenado o valor do LDR
int valorSensor;

void setup() {
  // Coloca variavel luz é um sinal de saida
  pinMode(luz, OUTPUT);

  // Coloca a variavel LDR como entrada
  pinMode(ldr, INPUT);

  // inicializa a comunicação serial com a taxa de 9600 bps
  Serial.begin(9600);
}

void loop() {
  // Faz a leitura do pino analógico LDR e
  // armazena o valor na variavel valorSensor
  valorSensor = analogRead(ldr);

  // Mostra o valor no monitor serial
  Serial.print("LDR : " );
  Serial.println(valorSensor);

  // Se o valor de valorSensor for menos que 500
  if (valorSensor > 200) {
    // Acende o LED
    digitalWrite(luz, HIGH);
  } else {
    // Se não apaga o LED
    digitalWrite(luz, LOW);
  }

  // Aguarda 100 milissegundos
  delay(100);
}